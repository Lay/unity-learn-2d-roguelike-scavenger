﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

// Lays out randomly generated levels each time the player starts a new level, based on the current level number
public class BoardManager : MonoBehaviour
{
    [Serializable]
    public class Count
    {
        public int minimum;
        public int maximum;

        public Count(int min, int max)
        {
            minimum = min;
            maximum = max;
        }
    }

    public int columns = 8;
    public int rows = 8;
    public Count wallCount = new Count(5, 9);
    public Count foodCount = new Count(1,5);
    public GameObject exit;
    public GameObject[] floorTiles;
    public GameObject[] wallTiles;
    public GameObject[] foodTiles;
    public GameObject[] enemyTiles;
    public GameObject[] outerWallTiles;

    private Transform boardHolder; // to keep the hierarchy clean
    private List<Vector3> gridPositions = new List<Vector3>();

    void InitialiseList()
    {
        gridPositions.Clear();

        // Loop game board grid, excluding a 1 tile layer around the grid to leave empty (where the exit sign is as well)
        // The grid is only where we place wells and food
        for (int x = 1; x < columns - 1; x++)
        {
            for (int y = 1; y < rows - 1; y++)
            {
                gridPositions.Add(new Vector3(x, y, 0f));
            }
        }
    }

    // setup outer wall tiles and background floor tiles
    void BoardSetup()
    {
        boardHolder = new GameObject("Board").transform;

        // Loop grid, including outer walls (-1 to rows|column + 1)
        for (int x = -1; x < columns + 1; x++)
        {
            for (int y = -1; y < rows + 1; y++)
            {
                // Get a random floor tile, unless it's an outerwall, then get a random outerwall tile
                GameObject toInstantiate = floorTiles[Random.Range(0, floorTiles.Length)];
                if (x == -1 || x == columns || y == -1 || y == rows)
                    toInstantiate = outerWallTiles[Random.Range(0, outerWallTiles.Length)];

                // Instanciate new game obejct with the tile, at the current (x,y) position. Quaternion.identity means instantiated with no rotation
                GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
                instance.transform.SetParent(boardHolder);
            }
        }
    }

    Vector3 RandomPosition()
    {
        int randomIndex = Random.Range(0, gridPositions.Count);
        Vector3 randomPosition = gridPositions[randomIndex];
        gridPositions.RemoveAt(randomIndex); // ensure we don't place 2 differents element in the same position
        return randomPosition;
    }

    void LayoutObjectAtRandom(GameObject[] tileArray, int minimum, int maximum)
    {
        int objectCount = Random.Range(minimum, maximum + 1);

        for (int i = 0; i < objectCount; i++)
        {
            // pick a random location to put object
            Vector3 randomPosition = RandomPosition();
            // pick random tile/sprite to use for this object
            GameObject tileChoice = tileArray[Random.Range(0, tileArray.Length)];
            // instantiate the game object
            Instantiate(tileChoice, randomPosition, Quaternion.identity);
        }
    }

    public void SetupScene(int level)
    {
        BoardSetup();
        InitialiseList();
        LayoutObjectAtRandom(wallTiles, wallCount.minimum, wallCount.maximum);
        LayoutObjectAtRandom(foodTiles, foodCount.minimum, foodCount.maximum);

        // Scale number of enemies based on the current level number, using Logarithmic difficulty progression
        // Means we will have 1 enemy at level 1, 2 enemies at level 4, 3 enemies at level 8, etc...
        int enemyCount = (int)Mathf.Log(level, 2f); // cast returned float to int
        LayoutObjectAtRandom(enemyTiles, enemyCount, enemyCount);

        // Place exit sign always at the same location - top right
        Instantiate(exit, new Vector3(columns -1, rows - 1, 0f), Quaternion.identity);
    }
}
